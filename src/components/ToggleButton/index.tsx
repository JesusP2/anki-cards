import React, { ReactNode, useState } from 'react';
import DefaultIcon from './Icons';

interface Props {
    model: boolean;
    onChange: (e: React.MouseEvent) => void;
    checkedIcon?: React.ReactNode;
    uncheckedIcon?: React.ReactNode;
}

function Button({ model, onChange }: Props) {
    return (
        <div onClick={onChange} className="cursor-pointer w-12 h-6 bg-zinc-300 dark:bg-zinc-700 border border-gray-700 rounded-full relative">
            <div className={`cursor-pointer w-[50%] rounded-full bg-base-100 grid place-items-center h-full absolute transition-all duration-300 easy-in ${model ? 'left-[50%]' : 'left-0'}`}>
                <DefaultIcon checked={model} /> 
            </div>
        </div>
    );
}
export default Button;

