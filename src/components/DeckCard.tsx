import Button from './Button';
import { Deck, useDeck } from 'contexts/Decks';
import { urlFor } from 'sanity';
import { Selector } from 'types';

export function DeckCard({ deck }: { deck: Deck }) {
    const imageUrl = deck.mainImage ? urlFor(deck.mainImage).url() : ''
    const title = deck.deck
    
    const {setCurrentDeck, setCurrentCard, setModifyDisplaySelector } = useDeck();

    //TODO: If image is empty, use a default image(relative path)
    function cardClickHandler(e?: React.MouseEvent) {
        setCurrentDeck(deck);
        setModifyDisplaySelector(Selector.updateDeck)
        setCurrentCard(null)
    }

    return (
        <>
            <Button
                onClick={cardClickHandler}
                className="flex w-full px-4 h-14 bg-primary mb-1 justify-between items-center rounded-sm"
            >
                <img src={imageUrl} alt={title} className="w-10 h-10 rounded-md" />
                <div className="justify-left flex-1 flex items-center">
                    <h3 className="pl-4 text-left">{title}</h3>
                </div>
            </Button>
        </>
    );
}
