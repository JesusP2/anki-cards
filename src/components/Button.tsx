import React, { ReactNode } from 'react';

interface Props {
    id?: string;
    children?: ReactNode;
    className?: string;
    ripple?: boolean;
    rippleColor?: string; //background-color class
    onClick?: (e?: React.MouseEvent) => void;
}

export default function Button({ id, children, className, onClick, ripple, rippleColor }: Props) {
    const useRipple = ripple === undefined ? true : ripple
    const defaultClass = useRipple ? 'relative overflow-hidden shadow-lg border-0' : 'shadow-lg border-0'
    function createRipple(event: React.MouseEvent) {
        event.stopPropagation()
        if (useRipple) {
            const button = event.currentTarget as HTMLElement;
            const circle = document.createElement('span');
            const diameter = Math.max(button.clientWidth, button.clientHeight);
            const radius = diameter / 2;

            circle.style.width = circle.style.height = `${diameter}px`;
            circle.style.left = `${event.clientX - button.offsetLeft - radius}px`;
            circle.style.top = `${event.clientY - button.offsetTop - radius}px`;
            if (rippleColor) {
                circle.classList.add('scale-0', 'animate-ripple', 'absolute', 'rounded-full', rippleColor, 'bg-opacity-70');
            } else {
                circle.classList.add('scale-0', 'animate-ripple', 'absolute', 'rounded-full', 'bg-white', 'bg-opacity-70');
            }

            const ripple = button.getElementsByClassName('animate-ripple')[0];

            if (ripple) {
                ripple.remove();
            }
            button.appendChild(circle);
        }
        if (onClick) {
            onClick(event)
        }
    }

    return (
        <button id={id} className={defaultClass + ' ' + className} onClick={createRipple}>
            {children}
        </button>
    );
}

