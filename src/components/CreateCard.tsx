import { useDeck } from 'contexts/Decks';
import { useState } from 'react';
import Title from './Title';
import MainImage from './MainImage';
import Description from 'components/Description';
import UpdateControlPanel from './UpdateControlPanel';
import { AlertTypes } from 'types';

export default function CreateCard() {
    const { createCard, addAlert } = useDeck();
    const [title, setTitle] = useState("");
    const [file, setFile] = useState<File | null>(null);
    const [description, setDescription] = useState("");
    const [imgUrl, setImgUrl] = useState("");
    
    async function updateHandler() {
        //TODO: Activate only when fields are complete.
        try {
            await createCard(title,  description, file as File)
            addAlert("Carta creada correctamente!", "", AlertTypes.success)
        } catch (err: unknown) {
            addAlert("Ocurrió un problema", "Vuelve a intentarlo", AlertTypes.error)
        }
        setTitle("")
        setFile(null)
        setDescription("")
        setImgUrl("")
    }
    return (
        <div className="flex flex-col h-full">
            <div className="w-[35rem] mx-auto py-8 flex-1">
                <Title title={title} setTitle={setTitle} />
                <MainImage imgUrl={imgUrl} setImgUrl={setImgUrl} setFile={setFile} />
                <Description description={description} setDescription={setDescription}  />
            </div>
                <UpdateControlPanel updateHandler={updateHandler} />
        </div>
    );
}

