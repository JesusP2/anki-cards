import { Deck, useDeck } from 'contexts/Decks';
import { urlFor } from 'sanity';
import { useEffect, useState } from 'react';
import Title from './Title';
import MainImage from './MainImage';
import UpdateControlPanel from './UpdateControlPanel';
import { AlertTypes } from 'types';

export default function UpdateDeck({
    currentDeck,
}: {
    currentDeck: Deck;
}) {
    const {updateDeck, addAlert } = useDeck()
    const [title, setTitle] = useState(currentDeck.deck);
    const [file, setFile] = useState<File | null>(null);
    const [imgUrl, setImgUrl] = useState(currentDeck.mainImage ? urlFor(currentDeck.mainImage).url() : '');
    
    useEffect(() => {
        setTitle(currentDeck.deck)
        setFile(null)
        setImgUrl(currentDeck.mainImage ? urlFor(currentDeck.mainImage).url() :  '')
    }, [currentDeck])
    async function updateHandler() {
        //TODO: Update only fields modified.
        try {
            await updateDeck({ deckId: currentDeck._id, image: file as File, name: title});
            addAlert("Datos actualizados correctamente", "", AlertTypes.success)
        } catch (err: unknown) {
            addAlert("Ocurrió un problema", "Vuelve a intentarlo", AlertTypes.error)
        }
        setTitle("")
        setFile(null)
    }

    return (
        <div className="flex flex-col h-full">
            <div className="w-[35rem] mx-auto py-8 flex-1">
                <Title title={title} setTitle={setTitle} />
                <MainImage imgUrl={imgUrl} setImgUrl={setImgUrl} setFile={setFile} />
            </div>
                <UpdateControlPanel updateHandler={updateHandler} />
        </div>
    );
}
