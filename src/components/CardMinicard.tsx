import Button from './Button';
import { Card, useDeck } from 'contexts/Decks';
import { urlFor } from 'sanity';
import { useState } from 'react';

export function CardMinicard({card, setCurrentCard}: { card: Card; setCurrentCard: () => void;}) {
    const imageUrl = card.mainImage ? urlFor(card.mainImage).url() : ''
    
    function cardClickHandler(e: any) {
        e.stopPropagation()
        setCurrentCard()
    }
    return (
        <>
            <Button
                onClick={cardClickHandler}
                className="flex w-full px-4 h-14 bg-primary mb-1 justify-between items-center rounded-sm"
            >
                <img src={imageUrl} alt={card.title} className="w-10 h-10 rounded-md" />
                <div className="justify-left flex-1 flex items-center">
                    <h3 className="pl-4 text-left">{card.title}</h3>
                </div>
            </Button>
        </>
    );
}

