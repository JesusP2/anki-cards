import { useDeck } from 'contexts/Decks';
import { useState } from 'react';
import Title from './Title';
import MainImage from './MainImage';
import UpdateControlPanel from './UpdateControlPanel';
import { AlertTypes } from 'types';

export default function CreateDeck() {
    const { createDeck, addAlert } = useDeck();
    const [title, setTitle] = useState(""); 
    const [file, setFile] = useState<File | null>(null);
    const [imgUrl, setImgUrl] = useState("");
    async function updateHandler() {
        //TODO: Activate only when fields are complete.
        try {
            await createDeck(title, file as File)
            addAlert("Deck Creada con exitosamente!", "", AlertTypes.success)
        } catch (err: unknown) {
            addAlert("Ocurrió un problema", "Vuelve a intentarlo", AlertTypes.error)
        }
        setTitle("")
        setFile(null)
        setImgUrl("")
    }
    return (
        <div className="flex flex-col h-full">
            <div className="w-[35rem] mx-auto py-8 flex-1">
                <Title title={title} setTitle={setTitle} />
                <MainImage imgUrl={imgUrl} setImgUrl={setImgUrl} setFile={setFile} />
            </div>
                <UpdateControlPanel updateHandler={updateHandler} />
        </div>
    );
}


