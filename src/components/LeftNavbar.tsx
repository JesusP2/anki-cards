import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSquarePlus } from '@fortawesome/free-regular-svg-icons';
import Button from './Button';
import { DeckCard } from './DeckCard';
import { useDeck } from 'contexts/Decks';
import { Selector } from 'types';

export default function LeftNavbar() {
    return (
        <>
            <DecksSection />
        </>
    );
}

function DecksSection() {
    const { decks, setModifyDisplaySelector } = useDeck();
    //TODO: Style scrollbar

    function createDeckHandler() {
        setModifyDisplaySelector(Selector.createDeck)
    }

    return (
        <div className="h-full overlflow-x-hidden border-r-[1px] border-0 border-gray-300 dark:border-gray-700  dark:focus:border-blue-600">
            <div className="flex px-4 h-12 text-xl items-center justify-between">
                <h2 className="font-bold">Decks</h2>
                <Button onClick={createDeckHandler} className="grid place-items-center">
                    <FontAwesomeIcon icon={faSquarePlus} className="h-6" />
                </Button>
            </div>
            <div className="px-2">
                {decks.map((deck, idx) => (
                    <DeckCard key={idx} deck={deck} />
                ))}
            </div>
        </div>
    );
}
