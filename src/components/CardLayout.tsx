import React from "react";

export default function CardLayout({children}: {children: React.ReactChild}) {
    return (
            <div className="bg-white text-slate-100 w-full h-full md:w-72 md:h-96 md:rounded-lg md:shadow-xl p-4">{children}</div>
    )
}
