import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEllipsisVertical } from '@fortawesome/free-solid-svg-icons';
import { useState } from 'react';
import { ReactSetState } from "types";

interface Props {
    imgUrl: string;
    setImgUrl: ReactSetState<string>;
    setFile: ReactSetState<File | null>;
}

export default function MainImage({
    imgUrl,
    setImgUrl,
    setFile
}: Props) {
    const [showImageClass, setShowImageSettings] = useState(false);

    function uploadHandler(e: React.ChangeEvent<HTMLInputElement>) {
        if (!e.target.files) return;
        const file = e.target.files[0]
        setFile(file)
        const reader = new FileReader();
        reader.readAsDataURL(file);

        reader.onload = (e) => {
            if (typeof e.target?.result === 'string') {
                setImgUrl(e.target.result);
            }
        };
    }

    function downloadHandler() {
        //TODO: Implement
        console.log('download handler');
    }

    function deleteHandler() {
        //TODO: Implement
        console.log('delete handler');
    }
    return (
        <div className="my-8">
            <label htmlFor="Image">
                <h1 className="text-sm font-bold">Image</h1>
            </label>
            <button className="dark:bg-stone-900 bg-gray-200 w-full h-72 relative outline focus:outline-2 focus:outline-blue-600 outline-1 outline-gray-400 dark:outline-gray-700 cursor-default mt-2 dark:focus:outline-blue-600">
                <img src={imgUrl} alt="" className="h-full mx-auto" />
                <div className="absolute w-8 h-8 bg-white dark:bg-black dark:border-gray-700 dark:text-gray-400 text-gray-500 hover:bg-zinc-700 hover:bg-opacity-80 hover:text-white border-gray-300 border rounded-sm mt-2 top-0 right-2">
                    <button
                        className="w-full h-full grid place-items-center"
                        onFocus={() => setShowImageSettings(true)}
                        //TODO: Reimplement this
                        onBlur={() => setTimeout(() => setShowImageSettings(false), 300)}
                    >
                        <FontAwesomeIcon icon={faEllipsisVertical} size="sm" />
                    </button>
                </div>
                <div
                    className={
                        'w-28 h-32 bg-white dark:bg-inherit absolute top-8 -right-20 rounded-md border border-gray-300 dark:border-gray-700 px-1 ' +
                        (showImageClass ? '' : 'hidden')
                    }
                >
                    <button className="text-zinc-600 dark:text-zinc-100 w-full mt-3 rounded-sm hover:bg-blue-600 hover:text-white py-1 text-left pl-4 overflow-hidden relative">
                        Subir
                        <input
                            type="file"
                            accept="image/"
                            onChange={uploadHandler}
                            className="absolute top-0 right-0 w-full h-full outline-none opacity-0 cursor-pointer"
                        />
                    </button>
                    <button
                        onClick={downloadHandler}
                        className="text-zinc-600 dark:text-zinc-100 w-full mt-1 rounded-sm hover:bg-blue-600 hover:text-white py-1 text-left pl-4"
                    >
                        Descargar
                    </button>
                    <button
                        onClick={deleteHandler}
                        className="text-red-700 dark:text-zinc-100 w-full mt-1 rounded-sm hover:bg-red-600 hover:text-white py-1 text-left pl-4"
                    >
                        Eliminar
                    </button>
                </div>
            </button>
        </div>
    );
}
