import { useDeck } from "contexts/Decks";
import { urlFor } from "sanity";

export default function DeckPageBanner() {
    const { currentDeck, groupCount } = useDeck();
    return (
        <>
            <div className="w-full h-64 overflow-hidden">
                <img
                    src={currentDeck?.mainImage ? urlFor(currentDeck.mainImage).url() : ''}
                    className="object-fit w-full"
                />
            </div>
        </>
    );
}

