export default function NoDeckView() {
    return (
        <div className='w-full'>
            <h1>Anki cards clone</h1>
            <p>Short personal project for creating for creating cards and decks to make learning things easier.</p>
        </div>
    )
}
