import { useDeck } from 'contexts/Decks';
import { useState } from 'react';
import Alert from './Alert';
import LeftNavbar from './LeftNavbar';
import Navbar from './Navbar';

export default function Layout({ children }: { children: React.ReactChild }) {
    const [leftNavbarDisplayClass, setLeftNavbarDisplayClass] = useState('block');
    const { alerts } = useDeck();
    return (
        <div className="flex flex-col h-screen">
            <Navbar
                leftNavbarDisplayClass={leftNavbarDisplayClass}
                setLeftNavbarDisplayClass={setLeftNavbarDisplayClass}
            />
            <div className="flex-row flex flex-1 h-[calc(100vh-3rem)]">
                <div className={'w-72 sticky flex-none ' + leftNavbarDisplayClass}>
                    <LeftNavbar />
                </div>
                <main className="w-full">{children}</main>
                <div className="absolute left-0 bottom-0 w-80">
                    {alerts?.map(({ id, title, description, type }) => (
                        <Alert key={id} title={title} description={description} type={type} />
                    ))}
                </div>
            </div>
        </div>
    );
}
