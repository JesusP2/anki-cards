import Button from './Button';
import React, { Fragment, ReactNode, Dispatch, SetStateAction } from 'react';

interface Options<T> {
    value: T;
    classNameActive?: string;
    classNameInactive?: string;
}

interface Props<T> {
    model: T;
    setModel: Dispatch<SetStateAction<T>>;
    options: Options<T>[];
    children: ReactNode[];
    dividers?: boolean;
    className?: string;
    classNameButtonActive?: string;
    classNameButtonInactive?: string;
    ripple?: boolean;
    rippleColor?: string;
    onClick?: (e?: React.MouseEvent) => void;
}

function ButtonGroup({ model, setModel, options, children, dividers, className, classNameButtonActive, classNameButtonInactive, onClick, ripple, rippleColor }: Props<any>) {
    function onClicked(index: number, e?: React.MouseEvent) {
        setModel(options[index].value);
        if (onClick) onClick(e);
    }

    const buttons = children.map((v, i) => {
        if (model === options[i].value) {
            const classNameInnerButtonActive = options[i].classNameActive ? classNameButtonActive + ' ' + options[i].classNameActive : classNameButtonActive;
            return (
                <Fragment key={i}>
                    <Button ripple={ripple} rippleColor={rippleColor} className={classNameInnerButtonActive}>
                        {v}
                    </Button>
                    {dividers && i < children.length - 1 ? <div className="divider py-0 my-0"></div> : <></>}
                </Fragment>
            );
        } else {
            const classNameInnerButtonInactive = options[i].classNameInactive
                ? classNameButtonInactive + ' ' + options[i].classNameInactive
                : classNameButtonInactive;
            return (
                <Fragment key={i}>
                    <Button
                        ripple={ripple}
                        rippleColor={rippleColor}
                        className={classNameInnerButtonInactive}
                        onClick={(e) => onClicked(i, e)}
                    >
                        {v}
                    </Button>
                    {dividers && i < children.length - 1 ? <div className="divider py-0 my-0"></div> : <></>}
                </Fragment>
            );
        }
    });
    return <div className={className ? className : ''}>{buttons}</div>;
}

export default ButtonGroup;

