import { useState } from "react";
import Button from "./Button";
import CardBack from "./CardBack";
import CardFront from "./CardFront";
import DifficultyButtons from "./DifficultyButtons";

export function DeckCardGame() {
    const [showAnswer, setShowAnswer] = useState(false);
    return (
        <div>
            {showAnswer ? <CardBack /> : <CardFront />}
            <div className="flex w-full justify-center mt-16">
                {showAnswer ? (
                    <DifficultyButtons />
                ) : (
                    <Button onClick={() => setShowAnswer(true)} className="p-4 shadow-none rounded-sm bg-accent">
                        Mostrar respuesta
                    </Button>
                )}
            </div>
        </div>
    );
}

