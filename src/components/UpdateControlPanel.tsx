import Button from "./Button";

export default function UpdateControlPanel({updateHandler}: {updateHandler: () => void;}) {
    return (
        <div className="h-16 w-full border-t-[1px] border-0 border-gray-300 dark:border-gray-700 dark:focus:border-blue-600 flex items-center justify-end">
            <Button
                onClick={() => updateHandler()}
                className="bg-green-600 hover:bg-green-700 text-white px-12 py-1 rounded-sm mr-4"
            >
                Actualizar
            </Button>
        </div>
    );
}
