import { useDeck } from 'contexts/Decks';
import Button from './Button';
import DeckPageBanner from './DeckPageBanner';
import { AlertTypes } from 'types';


export default function DeckPageIntro() {
    const { currentDeck, groupCount, setPlay, currentCard, addAlert } = useDeck();

    function clickHandler(e?: React.MouseEvent) {
        e?.stopPropagation()
        if (!currentCard) {
            addAlert("Felicidades!", "Haz terminado por hoy", AlertTypes.success)
            return;
        }
        setPlay(!!currentCard);
    }
    return (
        <>
            <DeckPageBanner />
            <div className="flex flex-col items-center gap-y-8">
                <h1 className="text-center font-bold text-4xl mt-4">{currentDeck?.deck}</h1>
                <h2 className="font-bold text-xl">Cards</h2>
                <div className="flex">
                    {Object.entries(groupCount)
                        .slice(0, 4)
                        .map(([k, v]) => (
                            <div key={k} className="mx-2">
                                <h3>{k}</h3>
                                <p className="text-center">{v}</p>
                            </div>
                        ))}
                </div>
                <Button
                    ripple={false}
                    onClick={clickHandler}
                    className="px-2 py-1 bg-blue-700 rounded-sm cursor-pointer"
                >
                    Iniciar
                </Button>
            </div>
        </>
    );
}
