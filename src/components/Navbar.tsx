import { Dispatch, SetStateAction, useContext, useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBars as faBarsSolid } from '@fortawesome/free-solid-svg-icons';
import Button from 'components/Button';
import ToggleButton from 'components/ToggleButton';
import colorModeContext from 'contexts/ColorMode';
import Link from 'next/link';

export default function Navbar({
    setLeftNavbarDisplayClass,
    leftNavbarDisplayClass,
}: {
    setLeftNavbarDisplayClass: Dispatch<SetStateAction<string>>;
    leftNavbarDisplayClass: string;
}) {
    const colorMode = useContext(colorModeContext);
    function toggleLeftNavbar() {
        setLeftNavbarDisplayClass(leftNavbarDisplayClass === 'hidden' ? 'block' : 'hidden');
    }
    return (
        <nav className="bg-secondary w-full h-12 sticky flex items-center">
            <div className="flex-1">
                <Button
                    className="h-12 w-12 focus:text-accent hover:text-accent justify-left"
                    onClick={toggleLeftNavbar}
                >
                    <FontAwesomeIcon icon={faBarsSolid} size="2x" />
                </Button>
            </div>
            <Link href="/">
                <a>
                    <Button className="h-12 mx-4 px-2 hover:bg-indigo-300">Cartas</Button>
                </a>
            </Link>
            <Link href="/modify">
                <a>
                    <Button className="h-12 mr-4 px-2">Editar</Button>
                </a>
            </Link>
            <div className="mr-12">
                <ToggleButton model={colorMode.colorMode === 'dark'} onChange={colorMode.toggleColorMode} />
            </div>
        </nav>
    );
}
