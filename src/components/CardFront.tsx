import { useDeck } from 'contexts/Decks';
import { urlFor } from 'sanity';
import CardLayout from './CardLayout';

export default function CardFront() {
    const { currentCard } = useDeck();
    return (
        <div className="mx-auto w-min mt-8">
            <CardLayout>
                <div>
                    <h2 className="text-xl text-amber-600 font-bold font-serif text-center">{currentCard?.title}</h2>
                    <img src={currentCard?.mainImage ? urlFor(currentCard?.mainImage).url() : ""} alt="" />
                </div>
            </CardLayout>
        </div>
    );
}
