import Button from "./Button";
import { Difficulty, useDeck } from "contexts/Decks";

export default function DifficultyButtons() {
    const {updateCard, currentCard} = useDeck()

    function clickHandler(diff: Difficulty) {
        if (diff === Difficulty.again) {
            updateCard({cardId: currentCard!._id, difficulty: diff, showAt: new Date(Math.floor(Date.now() + 60 * 1000)).toISOString()})
        } else if (diff === Difficulty.hard) {
            updateCard({cardId: currentCard!._id, difficulty: diff, showAt: new Date(Math.floor(Date.now() + 60 * 10 * 1000)).toISOString()})
        } else if (diff === Difficulty.normal) {
            updateCard({cardId: currentCard!._id, difficulty: diff, showAt: new Date(Math.floor(Date.now() + 60 * 60 * 24 * 1000)).toISOString()})
        } else if (diff === Difficulty.easy) {
            updateCard({cardId: currentCard!._id, difficulty: diff, showAt: new Date(Math.floor(Date.now() + 60 * 60 * 24 * 31 * 1000)).toISOString()})
        }


    }
    return (
        <>
            <Button
                onClick={() => clickHandler(Difficulty.again)}
                className="w-24 h-16 grid place-items-center text-red-500 border border-r-0 border-gray-400"
            >
                <p className="text-xs">1 min</p>
                <p>De nuevo</p>
            </Button>
            <Button
                onClick={() => clickHandler(Difficulty.hard)}
                className="w-24 h-16 grid place-items-center text-focus border border-r-0 border-gray-400"
            >
                <p className="text-xs">10 mins</p>
                <p>Dificil</p>
            </Button>
            <Button
                onClick={() => clickHandler(Difficulty.normal)}
                className="w-24 h-16 grid place-items-center text-blue-400 border border-r-0 border-gray-400"
            >
                <p className="text-xs">1 dia</p>
                <p>Normal</p>
            </Button>
            <Button
                onClick={() => clickHandler(Difficulty.easy)}
                className="w-24 h-16 grid place-items-center text-green-400 border border-gray-400"
            >
                <p className="text-xs">1 mes</p>
                <p>Facil</p>
            </Button>
        </>
    );
}

