import { Card, useDeck } from 'contexts/Decks';
import { urlFor } from 'sanity';
import { useEffect, useState } from 'react';
import Title from './Title';
import MainImage from './MainImage';
import Description from 'components/Description';
import UpdateControlPanel from './UpdateControlPanel';
import { AlertTypes } from 'types';

export default function UpdateCard({ currentCard }: { currentCard: Card }) {
    const { updateCard, addAlert } = useDeck();
    const [title, setTitle] = useState(currentCard.title);
    const [file, setFile] = useState<File | null>(null);
    const [description, setDescription] = useState(currentCard.description);
    const [imgUrl, setImgUrl] = useState(currentCard.mainImage ? urlFor(currentCard.mainImage).url() : '');
    
    useEffect(() => {
        setTitle(currentCard.title)
        setFile(null)
        setDescription(currentCard.description)
        setImgUrl(currentCard.mainImage ? urlFor(currentCard.mainImage).url() : '')
    }, [currentCard])
    async function updateHandler() {
        //TODO: Activate only when fields are modified and update only those fields.
        try {
            await updateCard({ cardId: currentCard._id, image: file as File, title, description });
            addAlert("Datos actualizados correctamente", "", AlertTypes.success)
        } catch (err: unknown) {
            addAlert("Ocurrió un problema", "Vuelve a intentarlo", AlertTypes.error)
        }
        setTitle("")
        setFile(null)
        setDescription("")
    }
    return (
        <div className="flex flex-col h-full">
            <div className="w-[35rem] mx-auto py-8 flex-1">
                <Title title={title} setTitle={setTitle} />
                <MainImage imgUrl={imgUrl} setImgUrl={setImgUrl} setFile={setFile} />
                <Description description={description} setDescription={setDescription}  />
            </div>
                <UpdateControlPanel updateHandler={updateHandler} />
        </div>
    );
}
