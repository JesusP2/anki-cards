import { Alert, AlertTypes, ReactSetState, Selector } from "types";
import React, { createContext, useContext, useEffect, useState } from 'react';
import { sanityClient } from 'sanity';

export enum Difficulty {
    none = 'none',
    easy = 'easy',
    normal = 'normal',
    hard = 'hard',
    again = 'again',
}

export type enumKeys = keyof Difficulty;

export type CardsDifficultyCount = {
    [key in Difficulty]: number;
};

export interface Deck {
    _createdAt: string;
    _id: string;
    _rev: string;
    _type: 'deck';
    _updatedAt: string;
    cardNumber: number;
    deck: string;
    mainImage?: {
        _type: string;
        asset: {
            _ref: string;
            _type: string;
        };
    };
}

export interface Card {
    _createdAt: string;
    _id: string;
    _rev: string;
    _type: 'card';
    _updatedAt: string;
    title: string;
    deck: {
        _ref: string;
        _type: string;
    };
    description: string;
    difficulty: Difficulty;
    showAt: string;
    mainImage?: {
        _type: string;
        asset: {
            _ref: string;
            _type: string;
        };
    };
}

export interface IMethods {
    currentDeck: Deck | null;
    setCurrentDeck: ReactSetState<Deck | null>;
    currentCards: Card[] | null;
    setCurrentCards: ReactSetState<Card[] | null>;
    decks: Deck[];
    setDecks: ReactSetState<Deck[]>;
    getDecks: () => Promise<Deck[]>;
    createDeck: (name: string, image?: File) => Promise<any>;
    updateDeck: (args: { deckId: string; name?: string; image: File }) => Promise<void>;
    cards: Card[];
    currentCard: Card | null;
    getCards: () => Promise<Card[]>;
    setCards: ReactSetState<Card[]>;
    createCard: (title: string, description: string, image?: File) => Promise<any>;
    updateCard: (args: {
        cardId: string;
        showAt?: string;
        title?: string;
        description?: string;
        difficulty?: Difficulty;
        image?: File;
    }) => Promise<void>;
    groupCount: CardsDifficultyCount;
    play: boolean;
    setPlay: ReactSetState<boolean>;
    setCurrentCard: ReactSetState<Card | null>;
    modifyDisplaySelector: Selector;
    setModifyDisplaySelector: ReactSetState<Selector>;
    alerts: Alert[];
    addAlert: (title: string, description: string, type: AlertTypes) => void;
}

const DeckContext = createContext<IMethods>({} as IMethods);

export function useDeck() {
    return useContext(DeckContext);
}

export function DeckProvider({ children }: { children: React.ReactNode }) {
    const [currentDeck, setCurrentDeck] = useState<Deck | null>(null);
    const [currentCards, setCurrentCards] = useState<Card[] | null>(null);
    const [currentCard, setCurrentCard] = useState<Card | null>(null);
    const [cards, setCards] = useState<Card[]>([]);
    const [decks, setDecks] = useState<Deck[]>([]);
    const [groupCount, setGroupCount] = useState<CardsDifficultyCount>({
        easy: 0,
        normal: 0,
        hard: 0,
        again: 0,
        none: 0,
    });
    const [play, setPlay] = useState(false);
    const [modifyDisplaySelector, setModifyDisplaySelector] = useState(Selector.none)
    const [alerts, setAlerts] = useState<Alert[]>([]);
    const [alertId, setAlertId] = useState(0);

    function addAlert(title: string, description: string, type: AlertTypes) {
        const _alerts = [...alerts, { id: alertId, title, description, type}];
        setAlerts(_alerts);
        setAlertId(alertId + 1);
    }

    async function getDecks(): Promise<Deck[]> {
        const query = `*[_type == 'deck']`;
        const decks = await sanityClient.fetch(query);
        setDecks(decks);
        return decks;
    }

    async function createDeck(name: string, image?: File) {
        const doc: Omit<Deck, '_createdAt' | '_id' | '_updatedAt' | '_rev'> = {
            _type: 'deck',
            deck: name,
            cardNumber: 0,
        };
        if (image) {
            const img = await sanityClient.assets.upload('image', image, { filename: image.name });
            doc['mainImage'] = {
                _type: 'image',
                asset: {
                    _ref: img._id,
                    _type: 'reference',
                },
            };
        }
        const deckCreated = await sanityClient.create(doc) as Deck
        const updatedDecks = [...decks].concat([deckCreated])
        setDecks(updatedDecks)
    }

    async function updateDeck({ deckId, name, image }: { deckId: string; name?: string; image?: File }) {
        const doc = {} as any;
        if (name) {
            doc['deck'] = name;
        }
        if (image && image.name) {
            const img = await sanityClient.assets.upload('image', image, { filename: image.name });
            doc['mainImage'] = {
                _type: 'image',
                asset: {
                    _ref: img._id,
                    _type: 'reference',
                },
            };
        }

        if (name || image) {
            const updatedDeck: Deck = await sanityClient.patch(deckId).set(doc).commit();
            let idx = -1;
            decks.filter((deck: Deck, index: number) => {
                if (deck._id === updatedDeck._id) {
                    idx = index;
                    return true
                }
                return false
            })
            const updatedDecks = [...decks.slice(0, idx), updatedDeck, ...decks.slice(idx + 1)]
            setDecks(updatedDecks);
        } else {
            throw new Error('No fields to update');
        }
    }

    // title: string, description: string, difficulty: Difficulty, showAt: string, mainImage: File
    async function updateCard({
        cardId,
        title,
        description,
        difficulty,
        showAt,
        image,
    }: {
        cardId: string;
        showAt?: string;
        title?: string;
        description?: string;
        difficulty?: Difficulty;
        image?: File;
    }) {
        const doc = {} as any;
        if (title) {
            doc['title'] = title;
        }
        if (description) {
            doc['description'] = description;
        }
        if (difficulty) {
            doc['difficulty'] = difficulty;
            doc['showAt'] = showAt;
        }
        if (image && image.name) {
            const img = await sanityClient.assets.upload('image', image, { filename: image.name });
            doc['mainImage'] = {
                _type: 'image',
                asset: {
                    _ref: img._id,
                    _type: 'reference',
                },
            };
        }

        if (Object.values(doc).length) {
            const updatedCard: Card = await sanityClient.patch(cardId).set(doc).commit();
            let idx = -1;
            cards.filter((card: Card, index: number) => {
                if (card._id === updatedCard._id) {
                    idx = index;
                    return true;
                }
                return false;
            });
            const updatedCards = [...cards.slice(0, idx), updatedCard, ...cards.slice(idx + 1)]
            const currentCards = updatedCards.filter((card) => card.deck._ref === currentDeck?._id);
            setCards(updatedCards);
            setCurrentCards(currentCards);
            setCurrentCard(updatedCard)
        } else {
            throw new Error('No fields to update');
        }
    }

    function getNextCard(cards: Card[]) {
        if (!currentCards) return;
        for (let card of cards) {
            const diffTimestamp = Math.floor(Date.now() / 1000) - Math.floor((new Date(card.showAt) as any) / 1000);
            if (diffTimestamp > 0) {
                setCurrentCard(card);
                return;
            }
        }
        setCurrentCard(null);
    }

    async function getCards(): Promise<Card[]> {
        const query = `*[_type == 'card']`;
        const cards = await sanityClient.fetch(query);
        setCards(cards);
        return cards;
    }

    async function createCard(title: string, description: string, image?: File) {
        if (!currentDeck) return;
        const doc: Omit<Card, '_createdAt' | '_id' | '_updatedAt' | '_rev' | 'showAt'> = {
            _type: 'card',
            title,
            description,
            difficulty: Difficulty.again,
            deck: {
                _ref: currentDeck._id,
                _type: 'reference',
            },
        };
        if (image) {
            const img = await sanityClient.assets.upload('image', image, { filename: image.name });
            doc['mainImage'] = {
                _type: 'image',
                asset: {
                    _ref: img._id,
                    _type: 'reference',
                },
            };
        }
        
        const cardCreated = await sanityClient.create(doc) as Card
        const updatedCards = [...cards].concat([cardCreated])
        const currentCards = updatedCards.filter((card) => card.deck._ref === currentDeck?._id);
        setCards(updatedCards);
        setCurrentCards(currentCards);
    }

    async function getCardsCountByDifficulty(cards: Card[]) {
        const count = { easy: 0, normal: 0, hard: 0, again: 0, none: 0 };
        cards.forEach(({ difficulty, deck }) => {
            if (currentDeck?._id === deck._ref) {
                count[difficulty]++;
            }
        });
        setGroupCount({ ...count });
    }
    const value: IMethods = {
        play,
        decks,
        cards,
        alerts,
        setPlay,
        setDecks,
        setCards,
        getDecks,
        getCards,
        addAlert,
        groupCount,
        createDeck,
        createCard,
        updateDeck,
        updateCard,
        currentCard,
        currentDeck,
        currentCards,
        setCurrentCard,
        setCurrentDeck,
        setCurrentCards,
        modifyDisplaySelector,
        setModifyDisplaySelector
    };

    useEffect(() => {
        getDecks();
        getCards();
    }, []);

    useEffect(() => {
        const currentCards = cards.filter((card) => card.deck._ref === currentDeck?._id);
        setCurrentCards(currentCards);
        getCardsCountByDifficulty(currentCards);
        setPlay(false);
    }, [currentDeck]);
    return <DeckContext.Provider value={value}>{decks.length && children}</DeckContext.Provider>;
}
