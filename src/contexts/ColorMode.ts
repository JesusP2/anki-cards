import {createContext} from 'react'
export interface ColorModeI {
    toggleColorMode: () => void;
    colorMode: 'dark' | 'light';
}
export default createContext<ColorModeI>({} as ColorModeI)
