export type ReactSetState<T> = React.Dispatch<React.SetStateAction<T>>;
export enum Selector {
    createCard = 'createCard',
    updateCard = 'updateCard',
    createDeck = 'createDeck',
    updateDeck = 'updateDeck',
    none = "none"
}

//FIXME: SOMEHOW THESE COLORS DON'T WORK
export enum AlertTypes {
    info = 'blue',
    success = 'emerald',
    warning = 'yellow',
    error = 'red'
}

export interface Alert {
    id: number;
    title: string;
    description: string;
    type: AlertTypes;
}
