import type { AppProps } from 'next/app';
import 'styles/tailwind.css';
import 'animate.css';
import Layout from 'components/Layout';
import { DeckProvider, useDeck } from 'contexts/Decks';
import { useEffect, useMemo, useState } from 'react';
import ColorModeContext, { ColorModeI } from 'contexts/ColorMode';

function MyApp({ Component, pageProps }: AppProps) {
    const [colorMode, setColorMode] = useState<'light' | 'dark'>('light');
    useEffect(() => {
        const localColorMode = localStorage.getItem('colorMode');
        if (localColorMode === 'light' || localColorMode === 'dark') {
            setColorMode(localColorMode);
            document.documentElement.setAttribute('data-theme', localColorMode);
            if (localColorMode === 'dark') {
                document.documentElement.classList.add('dark');
                return;
            }
            document.documentElement.classList.remove('dark');
            return;
        }
        localStorage.setItem('colorMode', 'light');
        document.documentElement.setAttribute('data-theme', ' light');
    }, [colorMode]);

    const colorModeContextMemo = useMemo<ColorModeI>(
        () => ({
            colorMode: colorMode,
            toggleColorMode: () => {
                const newColorMode = colorMode === 'light' ? 'dark' : 'light';
                setColorMode(newColorMode);
                localStorage.setItem('colorMode', newColorMode);
                document.documentElement.setAttribute('data-theme', newColorMode);
                if (newColorMode === 'dark') {
                    document.documentElement.classList.add('dark');
                    return;
                }
                document.documentElement.classList.remove('dark');
            },
        }),
        [colorMode],
    );
    return (
        <ColorModeContext.Provider value={colorModeContextMemo}>
            <DeckProvider>
                <Layout>
                    <Component {...pageProps} />
                </Layout>
            </DeckProvider>
        </ColorModeContext.Provider>
    );
}

export default MyApp;
