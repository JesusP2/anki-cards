import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSquarePlus } from '@fortawesome/free-regular-svg-icons';
import Button from 'components/Button';
import { Card, useDeck } from 'contexts/Decks';
import { CardMinicard } from 'components/CardMinicard';
import UpdateCard from 'components/UpdateCard';
import UpdateDeck from 'components/UpdateDeck';
import { Selector } from 'types';
import CreateDeck from 'components/CreateDeck';
import CreateCard from 'components/CreateCard';

export default function ModifyPage() {
    const { currentCards, currentDeck, currentCard, setCurrentCard, modifyDisplaySelector, setModifyDisplaySelector } = useDeck();
    
    function DisplaySelector() {
        switch(modifyDisplaySelector) {
            case Selector.updateCard:
                if (!currentCard) return;
                return <UpdateCard currentCard={currentCard} />;
            case Selector.updateDeck:
                if (!currentDeck) return
                return <UpdateDeck currentDeck={currentDeck} />
            case Selector.createDeck:
                return <CreateDeck />
            case  Selector.createCard:
                return <CreateCard />
            default:
                return <div>none</div>
        }
    }

    function setCardHandler(card: Card) {
        setCurrentCard(card)
        setModifyDisplaySelector(Selector.updateCard)
    }

    function createCardHandler() {
        setModifyDisplaySelector(Selector.createCard)
    }
    return (
        <div className="flex items-stretch h-full">
            <div
                className={
                    'overlflow-x-hidden w-80 border-r-[1px] border-0 border-gray-300 dark:border-gray-700  dark:focus:border-blue-600 ' +
                    (currentDeck ? '' : 'hidden')
                }
            >
                <div className="flex px-4 h-12 text-xl items-center justify-between">
                    <h2 className="font-bold">Cards</h2>
                    <Button onClick={createCardHandler} className="grid place-items-center">
                        <FontAwesomeIcon icon={faSquarePlus} className="h-6" />
                    </Button>
                </div>
                <div className="px-2">
                    {currentCards?.map((card) => (
                        <CardMinicard key={card._id} card={card} setCurrentCard={() => setCardHandler(card)} />
                    ))}
                </div>
            </div>
            <div className="flex-1">
                {DisplaySelector()}
            </div>
        </div>
    );
}
