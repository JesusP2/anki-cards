import NoDeckView from 'components/NoDeckView';
import { useDeck } from 'contexts/Decks';
import type { NextPage } from 'next';
import { DeckCardGame } from 'components/DeckCardGame';
import DeckPageIntro from 'components/DeckPageIntro';
import { useEffect } from 'react';

const Home: NextPage = () => {
    const { currentDeck,setCurrentDeck, setCurrentCard, setCurrentCards  } = useDeck();
    useEffect(() => {
        setCurrentCard(null)
        setCurrentDeck(null)
        setCurrentCards(null)
    }, [])
    return <>{currentDeck ? <DeckPage /> : <NoDeckView />}</>;
};

function DeckPage() {
    const {play} = useDeck()
    return <>
            { play ? <DeckCardGame /> : <DeckPageIntro />}
        </>;
}





export default Home;
