TODO: OVERALL
- Add notifications when important things occur (create a deck, a card, something fails, etc..)
- Add default image for cards and decks
- Finish the game cycle
- Define colors for night and light mode
- Try to make the website as responsive as possible
- Create extension for adding decks and cards
- Create mobile app (?) (check react native and alternatives)


TODO:
- Leftbar createbutton should only work if the path is /modify
- Check mainImage component 
- Move types and interfaces to types file
