// First, we must import the schema creator
import createSchema from 'part:@sanity/base/schema-creator';

// Then import schema types from any plugins that might expose them
import schemaTypes from 'all:part:@sanity/base/schema-type';

// Then we give our schema to the builder and provide the result to Sanity
export default createSchema({
    // We name our schema
    name: 'default',
    // Then proceed to concatenate our document type
    // to the ones provided by any plugins that are installed
    types: schemaTypes.concat([
        /* Your types here! */
        {
            title: 'Card',
            name: 'card',
            type: 'document',
            fields: [
                {
                    title: 'Title',
                    name: 'title',
                    type: 'string',
                },
                {
                    title: 'Deck',
                    name: 'deck',
                    type: 'reference',
                    to: [{ type: 'deck' }],
                },
                {
                    title: 'Main image',
                    name: 'mainImage',
                    type: 'image',
                    options: {
                        hotspot: true,
                    },
                },
                {
                    title: 'Description',
                    name: 'description',
                    type: 'text',
                },
                {
                    title: 'Difficulty',
                    name: 'difficulty',
                    type: 'string',
                    validation: (Rule) =>
                        Rule.required().custom((difficulty) => {
                            const isValid =
                                difficulty === 'again' ||
                                difficulty === 'hard' ||
                                difficulty === 'easy' ||
                                difficulty === 'super-easy' ||
                                'none';
                            return isValid
                                ? true
                                : "difficulty must be one of these values: 'again', 'hard', 'easy', 'super-easy' or 'none'.";
                        }),
                },
                {
                    title: 'Show at',
                    name: 'showAt',
                    type: 'datetime',
                },
            ],
        },
        {
            title: 'Deck',
            name: 'deck',
            type: 'document',
            fields: [
                {
                    title: 'Deck',
                    name: 'deck',
                    type: 'string',
                },
                {
                    title: 'Number of cards',
                    name: 'cardNumber',
                    type: 'number'
                },
                {
                    title: 'Main image',
                    name: 'mainImage',
                    type: 'image',
                    options: {
                        hotspot: true,
                    },
                },
            ],
        },
    ]),
});
